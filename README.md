# CIL_sentiment_analysis_2023

## Project Description
This project contains an attempt to perform sentiment analysis on a twitter dataset. This was done as part of the computational intelligence lab lecture of 2023 at ETH Zurich by Lea Künstler, Alexander Morgenroth and Anlin Yan.

As training data we have 2.5M tweets that originally contained either a positive smiling face :), or a negative smiling face :(. These were removed and the goal is to predict which of these were originally in the tweet, given the rest of the tweets content.

In a first step, data pre-processing is performed to handle some of the datas idiosyncracies like hashtags, misspellings, etc.

Then an array of state-of-the-art transformer models are used to perform the sentiment analysis.

As a useful comparison, we have also performed the same task, using an array of widely used models, which we use as a baseline for comparison.

## Directory structure
* raw_data - contains the input .txt files
* data_processing_scripts - contains the python scripts used to generate our data sets
* processed_data - contains the above mentioned data sets
* transformers_scripts - contains the BERT code
* transformers_output - empty directory, where BERT output is written
* baseline_scripts - contains the baseline code
* baseline_outputs - empty directory, where baseline output is written
* report - contains the project report

* tweet_analysis - contains some additional code to generate some appendix figures

## Running transformer models on Euler
Make sure that you are running on the new software stack by running this command: 

> `set_software_stack.sh new`

and logging out and back in.

To run a transformer model, first you must select hyper-parameters to use in the config section of transformers_training.py

### Hyperparameters to set:
- MODEL - choose which pre-trained model to fine-tune
- FINE_TUNE_TOKENIZER - binary variable to decide if you wish to - fine-tune the models tokenizer
- TRAINING_SET_SIZE - size of subset of full training set used for training
- VALIDATION_SET_SIZE - size of subset of full validation set used for evaluation
- NR_TRAINING_EPOCHS - number of training epochs
- LEARNING_RATE - learning rate that is used to initialize Adam
- EVAL_STEPS - how often to evaluate performance using the validation set
- MAX_STEPS - optional parameter to set the maximum number of steps taken
- PREPROCESSING_LEVEL - choose preprocessing level to use as input dataset

- INPUT_DIRECTORY - do not change unless you have moved the data directory
- OUTPUT_DIRECTORY - save location of output.csv and training checkpoints

Once satisfied with the hyper-parameters set, run the model by running 

> `sbatch ./run_script.sh`

This will load all relevant modules and install all necessary packages and then run the training script.
To track progress use the command 

> `myjobs`

Note that depending on the choice of hyperparameters, especially the size of the training set and the number of steps performed, this can take a while to run.
You might need to edit some run_script.sh parameters if you want to run larger tests.

## Running baseline models

Within the training scripts, you can select the GloVe embedding by changing its path. Note that our directory only contains the embedding trained on Twitter data using 100 dimensions. You can find further embedding files here: https://nlp.stanford.edu/projects/glove/ . If you choose an other dimesion size, you also need to change the dimension variable in the code. Further, you can choose what preprocessing you want to use.

In the run_script.sh file, you can select which model to train by removing the comment around it. Otherwise, follow the steps as described in the previous section, skipping the part with the hyperparameters.
from numpy.random import seed
seed(42)
import random
random.seed(42)
import tensorflow as tf
tf.random.set_seed(42)

from loading_glove import read_glove
import pandas as pd
import numpy as np

from keras.utils import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Embedding, Bidirectional, LSTM

import os
import zipfile

#### MODIFY THIS #####

# path to GloVe embedding txt file (string)
glove_file = "./glove.twitter.27B/glove.twitter.27B.100d.txt"

# dimension of GloVe embedding vectors (depends on the file) (int)
dim = 100

# choose which kind of preprocessed data you want: 

'''
0 - raw data
1 - hashtags as words
2 - hashtag removed
3 - only hashtags
4 - contractions expend
5 - stopwords removed
6 - only hashtag as words
'''

data_version = 0

#### END ####

# get the GloVe embedding matrix and dictionary
embedding_matrix, keys_dict = read_glove(glove_file, dim)


### GET THE DATA

df_train = pd.read_csv('../processed_data/preprocessed_versions/' + str(data_version) + '_train.csv')
df_test = pd.read_csv('../processed_data/preprocessed_versions/' + str(data_version) + '_test.csv')

tweets_train = df_train["tweet"].values
labels_train = df_train["label"].values

tweets_test = df_test["tweet"].values
labels_test = df_test["label"].values

# map tweets to indices

def word_to_index(x):

    if x in keys_dict:
        return keys_dict[x]
    else:
        return 0

seq_train = []
seq_test = []

for tweet in tweets_train:

    if isinstance(tweet, float):
        tweet = str(tweet)

    t = tweet.split()
    seq_train.append(list(map(word_to_index, t)))

for tweet in tweets_test:

    if isinstance(tweet, float):
        tweet = str(tweet)

    t=tweet.split()
    seq_test.append(list(map(word_to_index, t)))

pad_seq_train = pad_sequences(seq_train, maxlen=140)
pad_seq_test = pad_sequences(seq_test, maxlen=140)



# LSTM

from keras.models import Sequential
from keras.layers import Dense, Embedding, Bidirectional, LSTM

# define model
lstm = Sequential()
lstm.add(Embedding(input_dim=len(keys_dict), output_dim=dim, weights = [embedding_matrix], input_length=140, trainable=False))
lstm.add(Bidirectional(LSTM(100)))
lstm.add(Dense(32, activation = 'relu'))
lstm.add(Dense(1, activation='sigmoid'))
lstm.compile(optimizer='adam',loss='binary_crossentropy',metrics = ['accuracy'])

# train model
history = lstm.fit(pad_seq_train, labels_train, epochs=3, batch_size=256, validation_split=0.2,verbose=1)

# predict test set

y_pred = lstm.predict(pad_seq_test)
y_cat = np.where(y_pred < 0.5, -1, 1 )


# write prediction to csv file
ids = np.arange(1, y_cat.size +1)
y_predx = y_cat.reshape(-1)

df = pd.DataFrame({'Id': ids, 'Prediction': y_predx})
df.to_csv('../baseline_outputs/predictions_lstm.csv', index=False)

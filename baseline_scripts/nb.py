from numpy.random import seed
seed(42)
import random
random.seed(42)
import tensorflow as tf
tf.random.set_seed(42)

from loading_glove import read_glove
import pandas as pd
import numpy as np

from keras.utils import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Embedding, Bidirectional, LSTM

import os
import zipfile

#### MODIFY THIS #####

# path to GloVe embedding txt file (string)
glove_file = "./glove.twitter.27B/glove.twitter.27B.100d.txt"

# dimension of GloVe embedding vectors (depends on the file) (int)
dim = 100

# choose which kind of preprocessed data you want: 0-unprocessed

'''
0 - raw data
1 - hashtags as words
2 - hashtag removed
3 - only hashtags
4 - contractions expend
5 - stopwords removed
6 - only hashtag as words
'''

data_version = 0

#### END ####

# get the GloVe embedding matrix and dictionary
embedding_matrix, keys_dict = read_glove(glove_file, dim)


### GET THE DATA
df_train = pd.read_csv('../processed_data/preprocessed_versions/' + str(data_version) + '_train.csv')
df_test = pd.read_csv('../processed_data/preprocessed_versions/' + str(data_version) + '_test.csv')

tweets_train = df_train["tweet"].values
labels_train = df_train["label"].values

tweets_test = df_test["tweet"].values
labels_test = df_test["label"].values




# map tweets to indices

def word_to_index(x):

    if x in keys_dict:
        return keys_dict[x]
    else:
        return 0

seq_train = []
seq_test = []

for tweet in tweets_train:

    if isinstance(tweet, float):
        tweet = str(tweet)

    t = tweet.split()
    seq_train.append(list(map(word_to_index, t)))

for tweet in tweets_test:

    if isinstance(tweet, float):
        tweet = str(tweet)

    t=tweet.split()
    seq_test.append(list(map(word_to_index, t)))

pad_seq_train = pad_sequences(seq_train, maxlen=140)
pad_seq_test = pad_sequences(seq_test, maxlen=140)


# average each tweet over dimension

avg_emb_train = np.zeros((len(seq_train), dim))
avg_emb_test = np.zeros((len(seq_test), dim))

for i,t in enumerate(seq_train):

    c=0
    emb=np.zeros(dim)

    for w in t:
        
        if w!=0:

            c+=1
            w_vec = embedding_matrix[w]
            emb += w_vec
    
    if c!=0:

        div = c*np.ones(100)
        avg_emb_train[i] = emb/div
 

for i,t in enumerate(seq_test):

    c=0
    emb=np.zeros(dim)

    for w in t:
        
        if w!=0:

            c+=1
            w_vec = embedding_matrix[w]
            emb += w_vec
    
    if c!=0:

        div = c*np.ones(100)
        avg_emb_test[i] = emb/div

# LOGISTIC REGRESSION

from sklearn.naive_bayes import GaussianNB

# define model
nb = GaussianNB()

# fit model
nb.fit(avg_emb_train, labels_train)

# predict test set
y_pred = nb.predict(avg_emb_test)
y_cat = y_pred*2-1

# write prediction to csv file
ids = np.arange(1, y_cat.size +1)
y_predx = y_cat.reshape(-1)

df = pd.DataFrame({'Id': ids, 'Prediction': y_predx})
df.to_csv('../baseline_outputs/predictions_nb.csv', index=False)

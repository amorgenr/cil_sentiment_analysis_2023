# function returns embedding matrix and dictionary
# input: path to GloVe embedding txt file (string) and dimension of embedding vectors (int)

import numpy as np
from tqdm import tqdm

def read_glove(glovePath, dim):

    f = open(glovePath)

    embedding_vector = {} # dictionary that maps words to embedding vector

    for line in tqdm(f):

        value = line.split(' ')
        word = value[0]
        coefs = np.asarray(value[1:], dtype='float32')
        embedding_vector[word] = coefs

    # create dict that maps glove words to numbers -> starting at 1. 0 is equal to empty/unknown word

    keys_list = list(embedding_vector.keys())
    keys_dict = {}

    keys_dict[""] = 0

    for i, k in enumerate(keys_list):
        keys_dict[k] = i+1

    
    # create embedding matrix
    embedding_matrix = np.zeros((len(keys_list)+1, dim))

    for word in keys_dict:
        
        if word!= '':
            embedding_value = embedding_vector.get(word)
            embedding_matrix[keys_dict[word]] = embedding_value

    return embedding_matrix, keys_dict





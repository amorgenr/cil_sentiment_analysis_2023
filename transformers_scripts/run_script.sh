#!/bin/bash

#SBATCH -n 1
#SBATCH --cpus-per-task=1
#SBATCH --time=1:00:00
#SBATCH --mem-per-cpu=8192
#SBATCH --gpus=a100-pcie-40gb:1
#SBATCH --mail-type=END,FAIL

module load eth_proxy
module load gcc/8.2.0
module load python_gpu/3.11.2
module load hdf5/1.10.9
module load python/3.10.4

pip3 install --user -r requirements.txt

python3 transformers_training.py

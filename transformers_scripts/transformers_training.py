import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F

from datasets import load_dataset

from transformers import AutoTokenizer, AutoModelForSequenceClassification, TrainingArguments, Trainer

import evaluate


# set CUDA device
if torch.cuda.is_available():       
    device = torch.device("cuda")

else:
    print('No GPU available, using the CPU instead.')
    device = torch.device("cpu")


# set RNG for deterministic results
rng_seed = 1
torch.manual_seed(rng_seed)
np.random.seed(0)

# Choose pre-trained model to use
# MODEL = "bert-base-cased"  
MODEL = "bert-base-uncased"
# MODEL = "bert-large-cased"
# MODEL = "bert-large-uncased"
# MODEL = "roberta-base"
# MODEL = "roberta-large"

# Tokenizer
FINE_TUNE_TOKENIZER = False  # only tested with bert-base-uncased. For other models, need to change VOCAB_SIZE
VOCAB_SIZE = 30522
NR_TOKENS_TO_USE = 40

# Small training set size
TRAINING_SET_SIZE = 1000
VALIDATION_SET_SIZE = 10000

# other training hyperparameters
NR_TRAINING_EPOCHS = 1
LEARNING_RATE = 1e-05
EVAL_STEPS = 30000  # after how many steps to evaluate with validation set
MAX_STEPS = 100

# Pre-processing level (0 to 8 as string)
PREPROCESSING_LEVEL = "0"

# File parameters
INPUT_DIRECTORY = "../processed_data/preprocessed_versions/"
OUTPUT_DIRECTORY = "../transformers_output/"

dataset = load_dataset(
    "csv",
    data_files={'train': INPUT_DIRECTORY + PREPROCESSING_LEVEL + "_train.csv",
                'validation': INPUT_DIRECTORY + PREPROCESSING_LEVEL + "_validate.csv",
                "test": INPUT_DIRECTORY + PREPROCESSING_LEVEL + "_test.csv"})


# training data generator - to use in tokenizer fine tuning
def get_training_corpus():
    return (
        dataset["train"][i : i + 1000]["tweet"]
        for i in range(0, len(dataset["train"]), 1000)
    )

tokenizer = AutoTokenizer.from_pretrained(MODEL)
if FINE_TUNE_TOKENIZER:
    training_corpus = get_training_corpus()
    tokenizer = tokenizer.train_new_from_iterator(training_corpus, VOCAB_SIZE)


def tokenize_function(df):
    return tokenizer(df["tweet"], max_length = NR_TOKENS_TO_USE, 
                     padding="max_length", truncation=True)

tokenized_datasets = dataset.map(tokenize_function, batched=True)

small_train_dataset = (tokenized_datasets["train"]
                       .shuffle(seed=rng_seed)
                       .select(range(TRAINING_SET_SIZE)))
small_eval_dataset = (tokenized_datasets["validation"]
                      .shuffle(seed=rng_seed)
                      .select(range(VALIDATION_SET_SIZE)))


pretrained_model = AutoModelForSequenceClassification.from_pretrained(MODEL, num_labels=2)

training_args = TrainingArguments(output_dir=OUTPUT_DIRECTORY)

metric = evaluate.load("accuracy")

def compute_metrics(eval_pred):
    logits, labels = eval_pred
    predictions = np.argmax(logits, axis=-1)
    return metric.compute(predictions=predictions, references=labels)

training_args = TrainingArguments(
    output_dir="test_trainer", 
    evaluation_strategy="steps",  # to get clearer feedback, evaluation is fast, so this is ok
    report_to="none",  # default is report to weights and biases, unneccessary
    num_train_epochs=NR_TRAINING_EPOCHS,
    optim="adamw_torch",  # to get rid of error message (switch from adamw_hf to adamw_torch)
    learning_rate=LEARNING_RATE,  # issues with all same output for large training sets (>=140k) for default step size of 5e-05
    eval_steps=EVAL_STEPS,
    max_steps=MAX_STEPS,

    # Experimental arguments for speed-up
    # per_device_train_batch_size = 16,
    # per_device_eval_batch_size = 64
    # fp16=True
    # bf16=True
)  
training_args = training_args.set_save(strategy="steps", total_limit=2)  # so as to to not run out of disk space on larger/longer runs

trainer = Trainer(
    model=pretrained_model,
    args=training_args,
    train_dataset=small_train_dataset,
    eval_dataset=small_eval_dataset,
    compute_metrics=compute_metrics,
)

trainer.train()

# TODO: save metrics to csv file
predictions, labels, metrics = trainer.predict(tokenized_datasets["test"])

predictions_argmax = np.argmax(predictions, axis=-1)
predictions_corrected = 2*predictions_argmax - 1

np.savetxt(OUTPUT_DIRECTORY + 'output.csv', predictions_corrected, delimiter=',')
